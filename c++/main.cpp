/** @file main.cpp
 * Test for Space Applications candidates
 */

#include<dirent.h>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>

char getFolderSeparator()
{
  char sep = '/';
  #ifdef _WIN32
  sep = '\\';
  #endif
  return sep;
}

std::string getParentFolder(const std::string& s)
{
   size_t i = s.rfind(getFolderSeparator(), s.length());
   if (i != std::string::npos) {
      return(s.substr(0, i));
   }
   return("");
}

std::string createPath(const std::string& base_path, const std::string& folder)
{
  return std::string(base_path+getFolderSeparator()+folder);
}

void writeDetectionsToFile(const std::string &file_path, const std::string &detections)
{
  std::fstream file;
  file.open(createPath(file_path,"results.txt"), std::ios::out);
  file << detections<<std::endl;
  file.close();
}

std::vector<std::string> getImagesFromPath(const std::string &images_path)
{
  std::vector<std::string> files;
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir (images_path.c_str())) != NULL)
  {
    while ((ent = readdir (dir)) != NULL)
    {
      if (std::string(ent->d_name).size() > 3)
      {
        files.push_back(createPath(images_path, ent->d_name));
      }
    }
    closedir (dir);
  }
  else
  {
    perror ("Directory does not exist");
  }
  return files;
}


std::pair<cv::Point, cv::Point> detect(const cv::Mat& frame)
{
  /* Update this function with your detect logic */
  std::pair<cv::Point, cv::Point> bounding_box = std::make_pair(cv::Point(0, 0), cv::Point(100, 100)); // Initial value, to be updated

  // Add your code here

  return bounding_box;
}


int main(int argc, char** argv)
{
  /* Do not modify this function */
  std::string detections = "";
  std::string file_path = __FILE__;
  std::string images_path = file_path.substr(0, file_path.rfind("\\"));
  images_path = createPath(getParentFolder(getParentFolder(images_path)), "data/test_images");
  auto files = getImagesFromPath(images_path);

  for (auto file : files)
  {
    cv::Mat frame = cv::imread(file, CV_LOAD_IMAGE_COLOR);
    if (frame.empty() == false)
    {
      auto bounding_box = detect(frame);
      detections +=  file +" "+std::to_string(bounding_box.first.x) \
                          +" "+std::to_string(bounding_box.first.y) \
                          +" "+std::to_string(bounding_box.second.x) \
                          +" "+std::to_string(bounding_box.second.y) +"\n";

      cv::rectangle(frame, bounding_box.first, bounding_box.second, cv::Scalar(255, 0, 0), 3);
      cv::imshow("Detection", frame);
      if (cv::waitKey(0) == 'q')
      {
        break;
      }
    }
  }

  writeDetectionsToFile(getParentFolder(file_path), detections);
  return 0;
}
