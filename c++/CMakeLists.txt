cmake_minimum_required(VERSION 2.8 FATAL_ERROR)
project(test_candidates CXX C)

# Libraries
find_package( OpenCV REQUIRED )

# Executable
add_executable(main main.cpp)
target_link_libraries(main  ${OpenCV_LIBS})

# Add flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=c++14")
