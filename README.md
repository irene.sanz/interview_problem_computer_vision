## Programming test - Computer vision
Create the code to detect one of the cans in the images present in the data folder.
You may use either the c++ or the python structure. Make sure you have installed all necessary development packages to compile and execute c++ or python code, as well as opencv.


#### Steps to perform the test
1. Fork the repository to a private repository
2. Decide between C++ or python to perform the test
3. Install the necessary packages [instructions for Ubuntu 18.04]. Note this step could be done in a Docker container or directly on the development computer:
  1. C++: sudo apt install build-essential cmake pkg-config libopencv-dev
  2. Python:
    - sudo apt install python libopencv-dev python-opencv
    - sudo pip3 install opencv-python
  3. Install ROS 1: http://wiki.ros.org/melodic/Installation/Ubuntu  [install the latest ROS 1 version that works for your OS version]
4. Fill in the detect function with your code
5. Write a ROS wrapper to send the detected bbox via a ROS message
6. Share the link to your forked repo with us, including the results.txt file